﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Hangman_Swedish.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Hangman_Swedish.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Aardvark
        ///Albatross
        ///Alligator
        ///Alpacka
        ///Myra
        ///Myrslok
        ///Antilop
        ///Apa
        ///Bältdjur
        ///Åsna
        ///Babian
        ///Grävling
        ///Barracuda
        ///Fladdermus
        ///Björn
        ///Bäver
        ///Bi
        ///Bisonoxe
        ///Vildsvin
        ///Buffel
        ///Galago
        ///Fjäril
        ///kamel
        ///Caribou
        ///katt
        ///Tusenfoting
        ///Nötkreatur
        ///Gems
        ///Gepard
        ///Chimpans
        ///Chinchilla
        ///Alpkråka
        ///Mussla
        ///Kobra
        ///Kackerlacka
        ///Torsk
        ///Skarv
        ///Prärievarg
        ///Krabba
        ///Trana
        ///Krokodil
        ///Kråka
        ///Spov
        ///Dovhjort
        ///Dinosaurie
        ///Hund
        ///Pigghaj
        ///Delfin
        ///Fjällpipare
        ///Duva
        ///Trollslända
        ///Anka
        ///Dugong
        ///Kärrsnäppa
        ///Örn
        ///Myrpiggsvin
        ///Ål
        ///Eland
        ///Elefan [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string AnimalList {
            get {
                return ResourceManager.GetString("AnimalList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Guinness
        ///Newcastle Brown
        ///Sierra Nevada
        ///Stella Artois 
        ///Blue Moon
        ///Sapporo
        ///Samuel Adams
        ///Fat Tire
        ///Chimay
        ///Heineken
        ///Hoegaarden
        ///Stone IPA
        ///Pyramid Hefeweizen
        ///Bass Pale Ale
        ///Arrogant Bastard Ale
        ///Leffe Blonde
        ///Corona
        ///Murphy&apos;s Irish Stout
        ///Russian River
        ///Rogue Dead Guy Ale
        ///Weihenstephaner
        ///Anchor Steam
        ///Paulaner
        ///St Bernardus
        ///Rochefort
        ///Anchor Brewery
        ///Corona
        ///La Fin du Monde
        ///Yuengling
        ///Amstel
        ///Young&apos;s Luxury Double Chocolate Stout
        ///Delirium Tremens
        ///Red Stripe
        ///Goose Island Honkers Ale
        ///Magic Hat
        ///M [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string BeerList {
            get {
                return ResourceManager.GetString("BeerList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Buenos Aires
        ///Adelaide
        ///Brisbane
        ///Derrimut
        ///Melbourne
        ///Perth
        ///Sydney
        ///Graz
        ///Linz
        ///Wien
        ///Chittagong
        ///Dhaka
        ///Houdeng-Goegnies
        ///Oevel
        ///SCHOTEN
        ///Zaventem
        ///Gaborone
        ///Campinas
        ///Curitiba
        ///Guarulhos
        ///Paranagua
        ///Porto Alegre
        ///Rio De Janeiro
        ///Santos
        ///Sao Paulo
        ///Viracopos
        ///Phnom Penh
        ///Brampton
        ///Calgary
        ///Cambridge
        ///Edmonton
        ///Leamington
        ///Markham
        ///Mississauga
        ///Montreal
        ///Niagara Falls
        ///Vancouver
        ///Santiago
        ///Beijing
        ///Chengdu
        ///Chongqing
        ///Dalian
        ///Fuzhou
        ///Guangzhou
        ///Ningbo
        ///Qingdao
        ///Shanghai
        ///Shenyang
        ///Shenzhen
        ///Suzhou
        ///Tianji [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string CityList {
            get {
                return ResourceManager.GetString("CityList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Afghanistan
        ///Albanien
        ///Algeriet
        ///Andorra
        ///Angola
        ///Antigua och Barbuda
        ///Argentina
        ///Armenien
        ///Australien
        ///Azerbajdzjan
        ///Bahamas
        ///Bahrain
        ///Bangladesh
        ///Barbados
        ///Belgien
        ///Belize
        ///Benin
        ///Bhutan
        ///Bolivia
        ///Bosnien och Hercegovina
        ///Botswana
        ///Brasilien
        ///Brunei
        ///Bulgarien
        ///Burkina Faso
        ///Burundi
        ///Centralafrikanska republiken
        ///Chile
        ///Colombia
        ///Costa Rica
        ///Cypern
        ///Danmark
        ///Djibouti
        ///Dominica
        ///Dominikanska republiken
        ///Ecuador
        ///Egypten
        ///Ekvatorialguinea
        ///Elfenbenskusten
        ///El Salvador
        ///Eritrea
        ///Estland
        ///Etiopien
        ///Fijiöarna        /// [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string CountryList {
            get {
                return ResourceManager.GetString("CountryList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nyckeln till frihet
        ///Gudfadern
        ///The Dark Knight
        ///Pulp Fiction
        ///Den gode, den onde, den fule
        ///Schindler&apos;s List
        ///Tolv fördömda män
        ///Sagan om konungens återkomst
        ///Fight Club
        ///Ringens brödraskap
        ///Rymdimperiet slår tillbaka
        ///Interstellar
        ///Inception
        ///Forrest Gump
        ///Gökboet
        ///Sagan om de två tornen
        ///Goodfellas
        ///The Matrix
        ///Stjärnornas krig
        ///De sju samurajerna
        ///Guds stad
        ///Seven
        ///De misstänkta
        ///Närlammen tystnar
        ///C&apos;era una volta il West
        ///Leon
        ///Livet är underbart
        ///Casablanca
        ///Jakten på den försvunna skatten
        ///American H [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string MovieList {
            get {
                return ResourceManager.GetString("MovieList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Theodor Mommsen
        ///Björnstjerne Björnson
        ///Frederic Mistral
        ///Jose Echegaray
        ///Henryk Sienkiewicz
        ///Giosue Carducci
        ///Rudyard Kipling
        ///Rudolf Christoph Eucken
        ///Selma Lagerlöf
        ///Paul von Heyse
        ///Maurice Maeterlinck
        ///Gerhart Hauptmann
        ///Rabindranath Tagore
        ///Romain Rolland
        ///Verner von Heidenstam
        ///Karl Adolph Gjellerup
        ///Henrik Pontoppidan
        ///Carl Spitteler
        ///Knut Hamsun
        ///Anatole France
        ///Jacinto Benavente
        ///William Butler Yeats
        ///Wladyslaw Reymont
        ///George Bernard Shaw
        ///Grazia Deledda
        ///Henri Bergson
        ///Sigrid Undset
        ///Thomas Mann        /// [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string NobelList {
            get {
                return ResourceManager.GetString("NobelList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Andromeda
        ///Antlia
        ///Apus
        ///Aquarius
        ///Aquila
        ///Ara
        ///Aries
        ///Auriga
        ///Bootes
        ///Caelum
        ///Camelopardalis
        ///Cancer
        ///Canes Venatici
        ///Canis Major
        ///Canis Minor
        ///Capricornus
        ///Carina
        ///Cassiopeia
        ///Centaurus
        ///Cepheus
        ///Cetus
        ///Chamaeleon
        ///Circinus
        ///Columba
        ///Coma Berenices
        ///Corona Australis
        ///Corona Borealis
        ///Corvus
        ///Crater
        ///Crux
        ///Cygnus
        ///Delphinus
        ///Dorado
        ///Draco
        ///Equuleus
        ///Eridanus
        ///Fornax
        ///Gemini
        ///Grus
        ///Hercules
        ///Horologium
        ///Hydra
        ///Hydrus
        ///Indus
        ///Lacerta
        ///Leo
        ///Leo Minor
        ///Lepus
        ///Libra
        ///Lupus
        ///Lynx
        ///Lyra
        ///Mensa
        ///Microscopium
        ///Mono [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string StarList {
            get {
                return ResourceManager.GetString("StarList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Alabama
        ///Alaska
        ///Arizona
        ///Arkansas
        ///California
        ///Colorado
        ///Connecticut
        ///Delaware
        ///Florida
        ///Georgia
        ///Hawaii
        ///Idaho
        ///Illinois
        ///Indiana
        ///Iowa
        ///Kansas
        ///Kentucky
        ///Louisiana
        ///Maine
        ///Maryland
        ///Massachusetts
        ///Michigan
        ///Minnesota
        ///Mississippi
        ///Missouri
        ///Montana
        ///Nebraska
        ///Nevada
        ///New Hampshire
        ///New Jersey
        ///New Mexico
        ///New York
        ///North Carolina
        ///North Dakota
        ///Ohio
        ///Oklahoma
        ///Oregon
        ///Pennsylvania
        ///Rhode Island
        ///South Carolina
        ///South Dakota
        ///Tennessee
        ///Texas
        ///Utah
        ///Vermont
        ///Virginia
        ///Washington
        ///West Virginia
        ///Wisconsin        /// [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string StateList {
            get {
                return ResourceManager.GetString("StateList", resourceCulture);
            }
        }
    }
}
