﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Windows.Threading;

namespace Hangman_Swedish
{
    /// <summary>
    /// Interaction logic for Hangman.xaml
    /// </summary>
    public partial class Hangman : Window
    {
        private List<string> wordList = new List<string>();
        private Random r = new Random();
        private string word;
        private List<bool> found = new List<bool>();
        private List<char> used = new List<char>();
        private bool gameRunning = false, completed;
        private int hangStatus = 0;
        private Button clicked;
        private DispatcherTimer timer = new DispatcherTimer();
        private char[] separators = { ' ', '-', '\'', ':', '.', ',' };

        public Hangman()
        {
            InitializeComponent();
        }

        private void NewGame()
        {
            ClearStatus();     
       
            word = wordList[r.Next(wordList.Count)];
            
            for (int i = 0; i < word.Length; i++)           // If there are any separators in the word, those positions are set as found from the start.
            {
                found.Add(separators.Contains(word[i]) ? true : false);
            }

            UpdateWordStatus();

            gameRunning = true;
        }

        private void UpdateWordStatus()
        {
            string attempt = "";

            for (int i = 0; i < word.Length; i++)
            {
                attempt += found[i] ? word[i].ToString() : "_";
                
                if (i != word.Length - 1)
                    attempt += " ";
            }
            
            wordTextBlock.Text = attempt;
        }

        private void ClearStatus()
        {
            
            hangStatus = 0;
            found.Clear();
            used.Clear();
            completed = false;
            usedTextBlock.Text = "Använda bokstäver: ";
            DrawHangman(0);
        }

        private void InitiateWords(string resource)
        {
            wordList.Clear();
            
            using (StringReader reader = new StringReader(resource))
            {
                while (!(reader.Peek() == -1))
                {
                    wordList.Add(reader.ReadLine().ToUpper());
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            StartScreen start = new StartScreen();

            start.Owner = this;

            if ((bool)start.ShowDialog())
                this.Close();
            else
            {
                timer.Interval = new TimeSpan(1000000);
                timer.Tick += timer_Tick;

                InitiateWords(start.FileName);
            
                NewGame();
            }            

        }

        private void AddLetter(char c)
        {
            
            if (!used.Contains(c))
            {
                gameRunning = false;

                used.Add(c);

                usedTextBlock.Text += used.Count == 1 ? c.ToString() : ", " + c.ToString();
                
                if (word.Contains(c)) // Does the word contain the guessed letter?
                {
                    completed = true;

                    // This for loop runs through the entire word and sets the found bool Array to true for all instances of the guessed letter.
                    // If all letters are found, the bool completed will be true.

                    for (int i = 0; i < word.Length; i++)
                    {
                        if (word[i] == c)
                            found[i] = true;

                        if (!found[i])
                            completed = false;
                    }

                    UpdateWordStatus();
                    // If completed is true, the user has found the entire word and the game is over.
                    
                }
                else // If the guess is incorrect, the user gets a status update and the number of false guesses is increased by 1. The loop then starts over.
                {
                    hangStatus++;

                    DrawHangman(hangStatus);

                }
                
                if (completed)
                    GameOver("Du hittade ordet!");
                else if (hangStatus == 7)
                {
                    for (int i = 0; i < word.Length; i++ )
                        found[i] = true;

                    UpdateWordStatus();

                    GameOver("Tyvärr, gubben har blivit hängd.");
                }
                else
                    gameRunning = true;
            }

        }

        private void GameOver(string gameResult)
        {
            
            NewDialog playAgain = new NewDialog(gameResult, "Vill du spela igen?", "Ja", "Nej");
            playAgain.Owner = this;
            playAgain.Top = this.Top + 160;
            playAgain.Left = this.Left + 150;

            if ((bool) playAgain.ShowDialog())
                NewGame();
            else
                this.Close();
        }

        private void DrawHangman(int hangStatus)
        {
            
            switch (hangStatus)
            {
                case 0:
                    cnvHangman.Children.Clear();
                    return;

                case 1:
                    cnvHangman.Children.Add(new Line()
                    {
                        X1 = 150,
                        Y1 = 410,
                        X2 = 740,
                        Y2 = 410,
                        StrokeThickness = 20,
                        Stroke = Brushes.Black,
                        StrokeStartLineCap = PenLineCap.Round,
                        StrokeEndLineCap = PenLineCap.Round
                    });                  
                    
                    break;

                case 2:
                    cnvHangman.Children.Add(new Line()
                    {
                        X1 = 280,
                        Y1 = 30,
                        X2 = 280,
                        Y2 = 410,
                        StrokeThickness = 20,
                        Stroke = Brushes.Black,
                        StrokeStartLineCap = PenLineCap.Round
                    });

                    cnvHangman.Children.Add(new Line()
                    {
                        X1 = 200,
                        Y1 = 410,
                        X2 = 280,
                        Y2 = 330,
                        StrokeThickness = 15,
                        Stroke = Brushes.Black
                    });

                    cnvHangman.Children.Add(new Line()
                    {
                        X1 = 360,
                        Y1 = 410,
                        X2 = 280,
                        Y2 = 330,
                        StrokeThickness = 15,
                        Stroke = Brushes.Black
                    }); 
                    
                    break;

                case 3:

                    cnvHangman.Children.Add(new Line()
                    {
                        X1 = 280,
                        Y1 = 30,
                        X2 = 580,
                        Y2 = 30,
                        StrokeThickness = 20,
                        Stroke = Brushes.Black,
                        StrokeStartLineCap = PenLineCap.Round,
                        StrokeEndLineCap = PenLineCap.Round
                    });

                    cnvHangman.Children.Add(new Line()
                    {
                        X1 = 280,
                        Y1 = 110,
                        X2 = 360,
                        Y2 = 30,
                        StrokeThickness = 15,
                        Stroke = Brushes.Black
                    }); 

                    break;

                case 4:

                    cnvHangman.Children.Add(new Line()
                    {
                        X1 = 550,
                        Y1 = 30,
                        X2 = 550,
                        Y2 = 130,
                        StrokeThickness = 8,
                        Stroke = Brushes.Black
                    });

                    cnvHangman.Children.Add(new Ellipse()
                        {
                            Width = 60,
                            Height = 60,
                            Margin = new Thickness(520, 130, 0, 0),
                            StrokeThickness = 6,
                            Stroke = Brushes.Black
                        });

                    cnvHangman.Children.Add(new Line()
                    {
                        X1 = 538,
                        Y1 = 170,
                        X2 = 562,
                        Y2 = 170,
                        StrokeThickness = 4,
                        Stroke = Brushes.Black,
                        StrokeStartLineCap = PenLineCap.Round,
                        StrokeEndLineCap = PenLineCap.Round
                    });

                    cnvHangman.Children.Add(new Line()
                    {
                        X1 = 540,
                        Y1 = 150,
                        X2 = 540,
                        Y2 = 150,
                        StrokeThickness = 6,
                        Stroke = Brushes.Black,
                        StrokeStartLineCap = PenLineCap.Round,
                        StrokeEndLineCap = PenLineCap.Round
                    });

                    cnvHangman.Children.Add(new Line()
                    {
                        X1 = 560,
                        Y1 = 150,
                        X2 = 560,
                        Y2 = 150,
                        StrokeThickness = 6,
                        Stroke = Brushes.Black,
                        StrokeStartLineCap = PenLineCap.Round,
                        StrokeEndLineCap = PenLineCap.Round
                    });

                    break;

                case 5:

                    cnvHangman.Children.Add(new Line()
                    {
                        X1 = 550,
                        Y1 = 190,
                        X2 = 550,
                        Y2 = 290,
                        StrokeThickness = 10,
                        Stroke = Brushes.Black,
                        StrokeEndLineCap = PenLineCap.Round
                    });

                    break;

                case 6:

                    cnvHangman.Children.Add(new Line()
                    {
                        X1 = 550,
                        Y1 = 210,
                        X2 = 520,
                        Y2 = 260,
                        StrokeThickness = 8,
                        Stroke = Brushes.Black,
                        StrokeStartLineCap = PenLineCap.Round,
                        StrokeEndLineCap = PenLineCap.Round
                    });

                    cnvHangman.Children.Add(new Line()
                    {
                        X1 = 550,
                        Y1 = 210,
                        X2 = 580,
                        Y2 = 260,
                        StrokeThickness = 8,
                        Stroke = Brushes.Black,
                        StrokeStartLineCap = PenLineCap.Round,
                        StrokeEndLineCap = PenLineCap.Round
                    });

                    break;

                case 7:

                    cnvHangman.Children.Add(new Line()
                    {
                        X1 = 550,
                        Y1 = 290,
                        X2 = 520,
                        Y2 = 340,
                        StrokeThickness = 8,
                        Stroke = Brushes.Black,
                        StrokeStartLineCap = PenLineCap.Round,
                        StrokeEndLineCap = PenLineCap.Round
                    });

                    cnvHangman.Children.Add(new Line()
                    {
                        X1 = 550,
                        Y1 = 290,
                        X2 = 580,
                        Y2 = 340,
                        StrokeThickness = 8,
                        Stroke = Brushes.Black,
                        StrokeStartLineCap = PenLineCap.Round,
                        StrokeEndLineCap = PenLineCap.Round
                    });

                    break;

                default:
                    break;
            }

        }

        private void btn_Click(object sender, RoutedEventArgs e)
        {
            if (gameRunning)
            {
                Button b = (Button)sender;
                String s = b.Name.Substring(b.Name.Length - 1);
                Char c = s.ToCharArray()[0];

                Blink((Button) sender);

                AddLetter(c);
            }
        }
        
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            Key k = e.Key;
            
            if (k.ToString() == "Escape")
                btnMenu_Click(btnMenu, new RoutedEventArgs());
            
            if (gameRunning)
            {
                
                if (k.ToString().Length == 1 || k.ToString() == "Oem3" || k.ToString() == "Oem6" || k.ToString() == "OemQuotes")
                {
                    String c;
                    
                    switch (k.ToString())
                    {
                        case "Oem6":
                            c = "Å";
                            break;
                        case "OemQuotes":
                            c = "Ä";    
                            break;
                        case "Oem3":
                            c = "Ö";
                            break;
                        default:
                            c = k.ToString().ToUpper();
                            break;
                    }

                    Button b = grid.Children.OfType<Button>().ToList().Find(x => x.Name == "btn" + c);
                    
                    btn_Click(b, new RoutedEventArgs());    
                }

            }
        }
        private void Blink(Button b)
        {
            
            if (timer.IsEnabled)
                timer_Tick(timer, EventArgs.Empty);

            clicked = b;

            clicked.Background = Brushes.Maroon;
            clicked.Foreground = Brushes.Moccasin;

            timer.Start();

            
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            clicked.Foreground = Brushes.Maroon;
            clicked.Background = Brushes.Moccasin;

            (sender as DispatcherTimer).Stop();
        }

        private void btnMenu_Click(object sender, RoutedEventArgs e)
        {
            Window_Loaded(this, new RoutedEventArgs());
        }
    }
}
