﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Hangman_Swedish
{
    /// <summary>
    /// Interaction logic for StartScreen.xaml
    /// </summary>
    public partial class StartScreen : Window
    {
        private DispatcherTimer timer = new DispatcherTimer();
        private Button clicked;

        public StartScreen()
        {
            InitializeComponent();

            timer.Interval = new TimeSpan(1000000);
            timer.Tick += timer_Tick;
        }

        public string FileName { get; private set; }

        private void Countries_Click(object sender, RoutedEventArgs e)
        {
            Blink(sender);
            FileName = Properties.Resources.CountryList;
            
        }

        private void Cities_Click(object sender, RoutedEventArgs e)
        {
            Blink(sender);
            FileName = Properties.Resources.CityList;
            
        }

        private void Animals_Click(object sender, RoutedEventArgs e)
        {
            Blink(sender);
            FileName = Properties.Resources.AnimalList;
            
        }

        private void States_Click(object sender, RoutedEventArgs e)
        {
            Blink(sender);
            FileName = Properties.Resources.StateList;
            
        }

        private void Nobel_Click(object sender, RoutedEventArgs e)
        {
            Blink(sender);
            FileName = Properties.Resources.NobelList;
            
        }

        private void Stars_Click(object sender, RoutedEventArgs e)
        {
            Blink(sender);
            FileName = Properties.Resources.StarList;
            
        }

        private void Movies_Click(object sender, RoutedEventArgs e)
        {
            Blink(sender);
            FileName = Properties.Resources.MovieList;
           
        }

        private void Beers_Click(object sender, RoutedEventArgs e)
        {
            Blink(sender);
            FileName = Properties.Resources.BeerList;
            
        }

        private void Blink(object b)
        {

            if (timer.IsEnabled)
                timer_Tick(timer, new EventArgs());

            clicked = (Button)b;

            clicked.Background = Brushes.Maroon;
            clicked.Foreground = Brushes.Moccasin;

            timer.Start();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            clicked.Foreground = Brushes.Maroon;
            clicked.Background = Brushes.Moccasin;

            (sender as DispatcherTimer).Stop();

            this.Close();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            Key k = e.Key;
            int number;

            if (k.ToString() == "Escape")
            {
                this.DialogResult = true;
                this.Close();
            }
            else if (int.TryParse(k.ToString().Substring(k.ToString().Length - 1,1), out number))
            {
                RoutedEventArgs arg = new RoutedEventArgs();

                switch (number)
                {
                    case 1:
                        Countries_Click(btnCountries, arg);
                        break;
                    case 2:
                        Cities_Click(btnCities, arg);
                        break;
                    case 3:
                        Animals_Click(btnAnimals, arg);
                        break;
                    case 4:
                        States_Click(btnStates, arg);
                        break;
                    case 5:
                        Nobel_Click(btnNobel, arg);
                        break;
                    case 6:
                        Stars_Click(btnStars, arg);
                        break;
                    case 7:
                        Movies_Click(btnMovies, arg);
                        break;
                    case 8:
                        Beers_Click(btnBeers, arg);
                        break;
                    default:
                        break;
                }

                
            }    
            
        }
    }
}
